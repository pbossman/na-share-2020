'use strict'
const express = require('express');
const path = require('path');
const app = express();
const port = 8082;
const fs = require('fs');
const https = require('https');
const http = require('http');
const bodyParser = require('body-parser');
app.use(express.static(path.join(__dirname, 'public')));

// set common connection properties for Db2
exports.connObj = require('./config/config.json');
let options = {
    hostname: exports.connObj.HOSTNAME,
    port: exports.connObj.DB2APORT,
    method: 'POST',
    headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": "Basic " + exports.connObj.AUTH
    }
}

// Ensure every incomming request is parsed to json automatically
app.use(bodyParser.urlencoded({ extended: 'true' }));
app.use(bodyParser.json());

// serve web pages from public folder
app.use('/', express.static('public'));

//list customers
app.get('/customers', async function(req, res){
    console.log("Inside /customers");
    options.path = '/services/DSN8C10/listCustomersNSP';
    delete options.headers["Content-Length"];
    const reqDb2 = await http.request(options, (resDb2) => {
        let postData = ''; 
        resDb2.on('data', (chunk) => {
            postData += chunk;
            console.log('inside response db2 reqDb2', postData);
        });
        resDb2.on('end',() => {
            let result = JSON.parse(postData)['ResultSet 1 Output'].slice()
            console.log('test result', result)
            res.send(result);
        });
    });
    reqDb2.on('error', (err) => {
        console.log(err);
    })
    reqDb2.end();
});

//get customer
app.get('/customers/:id', async function(req, res){
    console.log("INFO: enter /customers/:id");
    let recId = parseInt(req.params.id,10);
    const inData = JSON.stringify({
        "HVCUSTOMERID": recId
      });
    options.path = '/services/DSN8C10/getCustomerNSP';
    options.headers["Content-Length"] = inData.length;
    const reqDb2 = await http.request(options, (resDb2) => {
        let postData = ''; 
        resDb2.on("data", (chunk) => {
            postData += chunk;
            console.log('INFO inside get customer data for id', postData);
        });
        resDb2.on('end',() => {
            let result = JSON.parse(postData)['ResultSet 1 Output'].slice()
            res.send(result);
        });
    }).write(inData);
    reqDb2.on('error', (err) => {
        console.log(err);
    })
    reqDb2.end();
});

//insert customers
app.post('/customers', async function(req, res){
    console.log("INFO: enter POST /customers/:id");
    let x = req.body;
    const inData = JSON.stringify({
        "HVCUSTOMERID": x.id,
        "HVFIRSTNAME": x.firstName, 
        "HVLASTNAME": x.lastName, 
        "HVSTREET_ADDRESS": x.streetAddress, 
        "HVCITY": x.city, 
        "HVSTATE": x.state,
        "HVZIP": x.zip, 
        "HVAGEGROUPID": x.ageGroupId, 
        "HVINCOME_RANGEID": x.incomeRangeId, 
        "HVMARITAL_STATUS": x.maritalStatus, 
        "HVGENDER": x.gender, 
        "HVETHNICGROUPID": x.ethnicGroupId
      });
    options.path = '/services/DSN8C10/postCustomerNSP';
    options.headers["Content-Length"] = inData.length;
    const reqDb2 = await http.request(options, (resDb2) => {
        let d = '';
        resDb2.on('data', (d) => {
            console.log('process post data');
          });
        resDb2.on('end',() => {
            res.send(d);
        });
    }).write(inData);
    
});

//put customers
app.put('/customers/:id', async function(req, res){
    console.log("INFO: enter PUT /customers/:id");
    let x = req.body;
    let recId = parseInt(req.params.id,10);
    const inData = JSON.stringify({
        "HVFIRSTNAME": x.firstName, 
        "HVLASTNAME": x.lastName, 
        "HVSTREET_ADDRESS": x.streetAddress, 
        "HVCITY": x.city, 
        "HVSTATE": x.state,
        "HVZIP": x.zip, 
        "HVAGEGROUPID": x.ageGroupId, 
        "HVINCOME_RANGEID": x.incomeRangeId, 
        "HVMARITAL_STATUS": x.maritalStatus, 
        "HVGENDER": x.gender, 
        "HVETHNICGROUPID": x.ethnicGroupId,
        "HVCUSTOMERID": recId 
      });
    options.path = '/services/DSN8C10/putCustomerNSP';
    options.headers["Content-Length"] = inData.length;
    const reqDb2 = await http.request(options, (resDb2) => {
        let d = '';
        resDb2.on('data', (d) => {
            console.log('process put data');
        });
        resDb2.on('end',() => {
            res.send(d);
        });
    }).write(inData);

});

//update customers
app.patch('/customers/:id', async function(req, res){
    console.log("INFO: enter PUT /customers/:id");
    let x = req.body;
    let recId = parseInt(req.params.id,10);
    const inData = JSON.stringify({
        "HVFIRSTNAME": x.firstName, 
        "HVLASTNAME": x.lastName, 
        "HVSTREET_ADDRESS": x.streetAddress, 
        "HVCITY": x.city, 
        "HVSTATE": x.state,
        "HVZIP": x.zip, 
        "HVAGEGROUPID": x.ageGroupId, 
        "HVINCOME_RANGEID": x.incomeRangeId, 
        "HVMARITAL_STATUS": x.maritalStatus, 
        "HVGENDER": x.gender, 
        "HVETHNICGROUPID": x.ethnicGroupId,
        "HVCUSTOMERID": recId 
      });
    options.path = '/services/DSN8C10/putCustomerNSP';
    options.headers["Content-Length"] = inData.length;
    const reqDb2 = await http.request(options, (resDb2) => {
        let d = '';
        resDb2.on('data', (d) => {
            console.log('process patch data');
        });
        resDb2.on('end',() => {
            res.send(d);
        });
    }).write(inData);

});

//delete  customer
app.delete('/customers/:id', async function(req, res){
    console.log("Inside DELETE /customers/:id");
    let recId = parseInt(req.params.id,10);
    const inData = JSON.stringify({
        "HVCUSTOMERID": recId
      });
    options.path = '/services/DSN8C10/deleteCustomerNSP';
    options.headers["Content-Length"] = inData.length;
    const reqDb2 = await http.request(options, (resDb2) => {
        let d = '';
        resDb2.on('data', (d) => {
            console.log('process delete data');
        });
        resDb2.on('end',() => {
            res.send(d);
        });
    }).write(inData);

});
// ************************** End of customers API **************************

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
